import { applyMiddleware, verifyJwtToken, getServices } from 'middlewares-nodejs';
import {
    getContactsController,
    saveContactController,
    deleteContactController,
    updateContactController
} from '../controller/controller.js';

export const healthCheck = (event, context, callback) => {
    const response = {
        statusCode: 200,
        body: JSON.stringify(event)
    };

    callback(null, response);
}

export const getContacts = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken, getServices], getContactsController);
}

export const createContact = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken, getServices], saveContactController);
}

export const deleteContact = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken, getServices], deleteContactController)
}

export const updateContact = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken, getServices], updateContactController)
}