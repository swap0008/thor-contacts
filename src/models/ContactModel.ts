import { FirestoreClient } from './FirestoreClient.js';

export class ContactModel extends FirestoreClient {
    static FIRST_NAME = 'first_name';
    static LAST_NAME = 'last_name';
    static PHONE_NUMBERS = 'phone_numbers';
    static DEFAULT_PHONE = 'default_phone';
    static EMAIL_ADDRESSES = 'email_addresses';
    static ADDRESS = 'address';
    static NOTES = 'notes';
    static TYPE = 'type';
    static ID = 'id';
    static ACCOUNT_ID = 'account_id';

    constructor() {
        super('contacts');
    }
}

export default ContactModel;