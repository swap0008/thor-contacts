import * as Joi from '@hapi/joi';
import { ContactModel } from '../models/ContactModel.js';
import {
    EXPORT_SPECIFIC_LOCAL_TO_GOOGLE,
    EXPORT_SPECIFIC_GOOGLE_TO_GOOGLE,
    EXPORT_LOCAL_TO_GOOGLE,
    EXPORT_GOOGLE_TO_GOOGLE
} from '../constants/constants.js';

const getFields = () => {
    return {
        [ContactModel.FIRST_NAME]: Joi.string(),
        [ContactModel.LAST_NAME]: Joi.string(),
        [ContactModel.PHONE_NUMBERS]: Joi.array().items(Joi.string()),
        [ContactModel.EMAIL_ADDRESSES]: Joi.array().items(Joi.string()),
        [ContactModel.ADDRESS]: Joi.string(),
        [ContactModel.NOTES]: Joi.string(),
        [ContactModel.TYPE]: Joi.string(),
        [ContactModel.ACCOUNT_ID]: Joi.string(),
        accounts: Joi.array().items(Joi.string().email().allow('')),
        resourceName: Joi.string(),
        etag: Joi.string()
    };
};

export const saveSchema = (data: object) => {
    const fields = getFields();
    const schema = Joi.object({
        [ContactModel.FIRST_NAME]: fields[ContactModel.FIRST_NAME].required(),
        [ContactModel.LAST_NAME]: fields[ContactModel.LAST_NAME].required(),
        [ContactModel.PHONE_NUMBERS]: fields[ContactModel.PHONE_NUMBERS].required(),
        [ContactModel.EMAIL_ADDRESSES]: fields[ContactModel.EMAIL_ADDRESSES],
        [ContactModel.ADDRESS]: fields[ContactModel.ADDRESS].allow(''),
        [ContactModel.NOTES]: fields[ContactModel.NOTES].allow(''),
        [ContactModel.TYPE]: fields[ContactModel.TYPE].allow(''),
        [ContactModel.ACCOUNT_ID]: fields[ContactModel.ACCOUNT_ID].required(),
        accounts: fields.accounts
    });

    return schema.validate(data);
}

export const updateSchema = (data: object) => {
    const fields = getFields();
    const schema = Joi.object({
        [ContactModel.FIRST_NAME]: fields[ContactModel.FIRST_NAME],
        [ContactModel.LAST_NAME]: fields[ContactModel.LAST_NAME],
        [ContactModel.PHONE_NUMBERS]: fields[ContactModel.PHONE_NUMBERS],
        [ContactModel.EMAIL_ADDRESSES]: fields[ContactModel.EMAIL_ADDRESSES],
        [ContactModel.ADDRESS]: fields[ContactModel.ADDRESS].allow(''),
        [ContactModel.NOTES]: fields[ContactModel.NOTES].allow(''),
        [ContactModel.TYPE]: fields[ContactModel.TYPE].allow(''),
        accounts: fields.accounts,
        resourceName: fields.resourceName.allow(''),
        etag: fields.etag.allow(''),
        [ContactModel.ACCOUNT_ID]: fields[ContactModel.ACCOUNT_ID].required()
    });

    return schema.validate(data);
}

export const getContactsSchema = (data: object) => {
    const fields = getFields();
    const schema = Joi.object({
        [ContactModel.ACCOUNT_ID]: fields[ContactModel.ACCOUNT_ID].required(),
        accounts: fields.accounts
    });

    return schema.validate(data);
}


export const syncSchema = (data: object, type: string) => {
    let schema = Joi.object({});

    if (type === EXPORT_GOOGLE_TO_GOOGLE) {
        schema = Joi.object({
            'export_from': Joi.string().email().required(),
            'export_to': Joi.string().email().required()
        })
    } else if (type === EXPORT_LOCAL_TO_GOOGLE) {
        schema = Joi.object({
            'account_id': Joi.string().required(),
            'accounts': Joi.array().items(Joi.string()).required()
        })
    } else if (type === EXPORT_SPECIFIC_GOOGLE_TO_GOOGLE) {
        schema = Joi.object({
            'accounts': Joi.array().items(Joi.string()).required(),
            'google_contacts': Joi.array().required()
        })
    } else if (type === EXPORT_SPECIFIC_LOCAL_TO_GOOGLE) {
        schema = Joi.object({
            'accounts': Joi.array().items(Joi.string()).required(),
            'contact_ids': Joi.array().items(Joi.string()).required()
        })
    }

    return schema.validate(data);
}
