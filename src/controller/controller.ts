import { pick } from 'lodash';
import { ContactModel } from '../models/ContactModel.js';
import { saveSchema, getContactsSchema, updateSchema, syncSchema } from '../schema/schema.js';
import {
    EXPORT_GOOGLE_TO_GOOGLE,
    EXPORT_LOCAL_TO_GOOGLE,
    EXPORT_SPECIFIC_GOOGLE_TO_GOOGLE,
    EXPORT_SPECIFIC_LOCAL_TO_GOOGLE,
    DELETE_LOCAL_CONTACTS,
    DELETE_GOOGLE_CONTACTS,
    DELETE_MULTIPLE_CONTACTS,
    CAN_ACCESS_GOOGLE_CONTACTS,
    CAN_ACCESS_PLATFORM_CONTACTS,
} from '../constants/constants.js';

const Contact = new ContactModel();

const response = (statusCode, body) => {
    return {
        statusCode: statusCode || 200,
        body: JSON.stringify(body)
    };
}

const parseToPlatformContacts = (connections: any) => {
    const contacts: object[] = [];
    connections.forEach(({ names, emailAddresses = [], addresses = [], phoneNumbers = [], resourceName, etag }) => {
        const data = {
            [ContactModel.FIRST_NAME]: names[0].givenName || '',
            [ContactModel.LAST_NAME]: names[0].familyName || '',
            [ContactModel.PHONE_NUMBERS]: phoneNumbers.map((phone: any) => phone.value),
            [ContactModel.DEFAULT_PHONE]: phoneNumbers[0] ? phoneNumbers[0].value : '',
            [ContactModel.EMAIL_ADDRESSES]: emailAddresses.map((email: any) => email.value),
            [ContactModel.ADDRESS]: addresses.map((address: any) => address.streetAddress),
            [ContactModel.NOTES]: '',
            [ContactModel.TYPE]: 'google_contacts',
            resourceName,
            etag
        }

        contacts.push(data);
    });

    return contacts;
}

const parseToGoogleContact = (contact: object) => {
    let { first_name, last_name, phone_numbers, email_addresses, address, default_phone }: any = contact;
    let phoneNumbers = [{ value: default_phone }];
    phone_numbers.forEach(value => {
        if (value !== default_phone) phoneNumbers.push({ value });
    });

    return {
        names: [{ familyName: last_name, givenName: first_name }],
        addresses: Array.isArray(address) ? address.map(streetAddress => ({ streetAddress })) : [{ streetAddress: address }],
        phoneNumbers,
        emailAddresses: email_addresses.map(value => ({ value })),
    }
}

const deleteGoogleContact = async (service: any, resourceName) => {
    await service.people.deleteContact({ resourceName });
}

const updateGoogleContact = async (service: any, contact: any) => {
    const { resourceName, etag } = contact;

    const { data } = await service.people.updateContact({
        resourceName,
        updatePersonFields: 'names,addresses,emailAddresses,phoneNumbers',
        requestBody: {
            etag,
            ...parseToGoogleContact(contact)
        }
    });

    return data;
}

const createGoogleContact = async (service, contact) => {
    await service.people.createContact({
        requestBody: parseToGoogleContact(contact)
    });
}

const getGoogleContacts = async (service) => {
    const { data: { connections } } = await service.people.connections.list({
        resourceName: 'people/me',
        pageSize: 10,
        personFields: 'addresses,emailAddresses,names,phoneNumbers,taglines'
    });

    if (!connections) return [];

    return parseToPlatformContacts(connections)
}

export const getContactsController = async (event, callback) => {
    const { permissions } = event;

    const { account_id } = event.pathParameters || {};
    let { accounts = '' } = event.queryStringParameters || {};
    accounts = accounts.split(',');

    const { error } = getContactsSchema({ account_id, accounts });
    if (error) return callback(null, response(400, error.details[0].message));

    let contacts = {};

    if (accounts.length && permissions[CAN_ACCESS_GOOGLE_CONTACTS]) {
        const { services = [] } = event;
        for (const service of services) {
            const contact = await getGoogleContacts(service);
            for (let c of contact) {
                contacts[c.resourceName] = c;
            }
        }
    }

    if (permissions[CAN_ACCESS_PLATFORM_CONTACTS]) {
        const snapshot = await Contact.collectionRef.where(ContactModel.ACCOUNT_ID, '==', account_id).get();
        if (!snapshot.empty)
            snapshot.docs.map(doc => {
                const contact = doc.data();
                contacts[contact.id] = contact;
            });
        return callback(null, response(200, { contacts, total_platform_contacts: snapshot.docs.length }));
    }

    callback(null, response(403, 'Access Denied. You are not authorized to access platform or google contacts.'));
}

export const saveContactController = async (event, callback) => {
    const { permissions } = event;
    
    const { contact, accounts = [] } = event.body;

    const { error } = saveSchema({ ...contact, accounts });
    if (error) return callback(null, response(400, error.details[0].message));

    contact[ContactModel.DEFAULT_PHONE] = contact[ContactModel.PHONE_NUMBERS][0] || '';

    if (accounts.length && permissions[CAN_ACCESS_GOOGLE_CONTACTS]) {
        const { services = [] } = event;
        for (let service of services) {
            await createGoogleContact(service, contact);
        }

        return callback(null, response(200, 'Saved!'));
    }

    if (!accounts.length && permissions[CAN_ACCESS_PLATFORM_CONTACTS]) {
        const { id } = await Contact.collectionRef.doc();
        contact[ContactModel.ID] = id;
        await Contact.set(id, contact);
        contact.id = id;
        return callback(null, response(200,  contact));
    }

    callback(null, response(403, 'Access Denied. You are not authorized to access platform or google contacts.'));
}

const exportGoogleToGoogle = async (event, callback) => {
    const { requestContext: { apiId } } = event;
    let { export_from, export_to, account_id, accounts, type } = event.body;

    const { error } = syncSchema({ export_from, export_to }, type);
    if (error) return callback(null, response(400, error.details[0].message));

    if (export_from && export_to) accounts = [export_from, export_to];
    const { services = [] } = event;

    if (export_from && export_to) {
        if (!services[0]) return callback(null, response(400, `${export_from} acccount does not exists!`));
        const contacts = await getGoogleContacts(services[0]);

        if (!services[1]) return callback(null, response(400, `${export_to} acccount does not exists!`));
        for (const contact of contacts) await createGoogleContact(services[1], contact);

        return callback(null, response(200, `Contacts exported from ${export_from} to ${export_to}`));
    }
}

const exportSpecificGoogleToGoogle = async (event, callback) => {
    const { requestContext: { apiId } } = event;
    const { google_contacts, account_id, accounts, type } = event.body;

    const { error } = syncSchema({ accounts, google_contacts }, type);
    if (error) return callback(null, response(400, error.details[0].message));

    const { services = [] } = event;

    if (google_contacts) {
        for (const service of services) {
            try {
                for (const contact of google_contacts) {
                    await createGoogleContact(service, contact);
                };
            } catch (e) {
                console.log(e);
            }
        };

        return callback(null, response(200, 'Contacts exported!'));
    }
}

const exportSpecificLocalToGoogle = async (event, callback) => {
    const { contact_ids, account_id, accounts, type } = event.body;

    const { error } = syncSchema({ accounts, contact_ids }, type);
    if (error) return callback(null, response(400, error.details[0].message));

    const { services = [] } = event;

    const snapshot = await Contact.collectionRef.where(ContactModel.ID, 'in', contact_ids).get();
    if (snapshot.empty) return callback(null, response(400, 'No contact found!'));

    const contacts = snapshot.docs.map(doc => doc.data());

    for (const service of services) {
        try {
            for (const contact of contacts) {
                await createGoogleContact(service, contact);
            };
        } catch (e) {
            console.log(e);
        }
    };

    await Contact.deleteMultipleDoc(contact_ids, snapshot);

    return callback(null, response(200, 'Contacts exported!'));
}

const exportLocalToGoogle = async (event, callback) => {
    const { account_id, accounts, type } = event.body;

    const { error } = syncSchema({ account_id, accounts }, type);
    if (error) return callback(null, response(400, error.details[0].message));

    let query = Contact.collectionRef.where(ContactModel.ACCOUNT_ID, '==', account_id);

    const snapshot = await query.get();
    if (snapshot.empty) return callback(null, response(200, 'Nothing to sync!'));

    const contacts = snapshot.docs.map(doc => doc.data());
    const { services = [] } = event;

    let contactIds: string[] = [];
    for (const service of services) {
        try {
            for (const contact of contacts) {
                await createGoogleContact(service, contact);
                contactIds.push(contact.id);
            };
        } catch (e) {
            console.log(e);
        }
    };

    contactIds = [...new Set(contactIds)];
    if (contactIds.length !== 0) await Contact.deleteMultipleDoc(contactIds, null);
    callback(null, response(200, 'Sync Completed!'));
}

export const syncContacts = async (event, callback) => {
    const { permissions } = event;
    let { type } = event.body;

    switch (type) {
        case EXPORT_GOOGLE_TO_GOOGLE:
            if (!permissions[CAN_ACCESS_PLATFORM_CONTACTS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            return await exportGoogleToGoogle(event, callback);
        case EXPORT_SPECIFIC_GOOGLE_TO_GOOGLE:
            if (!permissions[CAN_ACCESS_PLATFORM_CONTACTS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            return await exportSpecificGoogleToGoogle(event, callback);
        case EXPORT_LOCAL_TO_GOOGLE:
            if (!permissions[CAN_ACCESS_GOOGLE_CONTACTS] || !permissions[CAN_ACCESS_PLATFORM_CONTACTS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            return await exportLocalToGoogle(event, callback);
        case EXPORT_SPECIFIC_LOCAL_TO_GOOGLE:
            if (!permissions[CAN_ACCESS_GOOGLE_CONTACTS] || !permissions[CAN_ACCESS_PLATFORM_CONTACTS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            return await exportSpecificLocalToGoogle(event, callback);
        default:
            callback(null, response(400, 'Bad Request.'));
    }
}

export const updateContactController = async (event, callback) => {
    const { permissions } = event;
    const { account } = event.queryStringParameters || {};
    const { account_id } = event.pathParameters || {};

    const accounts = [account];
    //resourceName and eTag are required to update google contact
    const contact = pick(event.body, [
        ContactModel.FIRST_NAME,
        ContactModel.LAST_NAME,
        ContactModel.PHONE_NUMBERS,
        ContactModel.EMAIL_ADDRESSES,
        ContactModel.ADDRESS,
        ContactModel.NOTES,
        ContactModel.TYPE,
        ContactModel.DEFAULT_PHONE,
        ContactModel.ID,
        'resourceName',
        'etag'
    ]);

    const { error } = updateSchema({ account_id, accounts, ...contact });
    if (error) return callback(null, response(400, error.details[0].message));

    if (account && permissions[CAN_ACCESS_GOOGLE_CONTACTS]) {
        const { services = [] } = event;
        const data = await updateGoogleContact(services[0], contact);

        return callback(null, response(200, data));
    }

    if (permissions[CAN_ACCESS_PLATFORM_CONTACTS]) {
        await Contact.update(contact[ContactModel.ID], contact);
        return callback(null, response(200, 'Contact updated!'));
    }

    callback(null, response(403, 'Access denied. You are not authorized.'));
}

const deleteLocalContacts = async (event) => {
    const { platform_contacts } = event.body;

    await Contact.deleteMultipleDoc(platform_contacts, null);
    return 'Platform contacts deleted!';
}

const deleteGoogleContacts = async (event, callback) => {
    const { account_id } = event.pathParameters;
    const { google_contacts } = event.body;

    const accounts = Object.keys(google_contacts);

    for (let account of accounts) {
        const { services = [] } = event;
        for (let resourceName of google_contacts[account])
            await deleteGoogleContact(services[0], resourceName);
    }

    return 'Google contacts deleted!';
}

export const deleteContactController = async (event, callback) => {
    const { permissions } = event;
    const { type } = event.body;

    let result: any = '';
    switch (type) {
        case DELETE_LOCAL_CONTACTS:
            if (!permissions[CAN_ACCESS_PLATFORM_CONTACTS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            result = await deleteLocalContacts(event);
            return callback(null, response(200, result));
        case DELETE_GOOGLE_CONTACTS:
            if (!permissions[CAN_ACCESS_GOOGLE_CONTACTS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            result = await deleteGoogleContacts(event, callback);
            return callback(null, response(200, result));
        case DELETE_MULTIPLE_CONTACTS:
            if (!permissions[CAN_ACCESS_GOOGLE_CONTACTS] || !permissions[CAN_ACCESS_PLATFORM_CONTACTS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            await deleteLocalContacts(event);
            await deleteGoogleContacts(event, callback);
            return callback(null, response(200, 'Contacts deleted!'));
        default:
            callback(null, response(400, 'Bad request.'));
    }
}